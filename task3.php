<html>
<title>Chess Board</title>
<head></head>
<body>


<form method="get">


    <?php
     {
    $n = 8;
    $m= $n*$n;
    $width = 400;
    ?>
    <div style="width:<?php echo $width; ?>">
        <br>
        <?php
        $line=1;
        for ($i = 1; $i <= $m; $i++) {
            if($i%2==0){
                $color="red";
            }
            else{
                $color="black";
            }
            if($line>1 && $line%2==0)
            {
                $float="right";
            }
            else
            {
                $float="left";
            }
            ?>
            <div
                style="width:50px;height:50px;float:<?php echo $float;?>;
                    border-style: none;color:white;background-color:<?php echo $color;?>;
                    font: 125%;text-align: center;margin:0px;">

            </div>
            <?php
            if($i%$n == 0)
            {
                $line++;
            }
        }
        }
        ?>
    </div>
</form>
</body>
</html>